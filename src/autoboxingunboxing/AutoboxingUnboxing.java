
package autoboxingunboxing;

/**
 *
 * @author tinix
 */
public class AutoboxingUnboxing {

    public static void main(String[] args) {
//        autoboxing
     Integer entereObj = 10;
     Float flotanteObj = 15.2f;
     Double dobleObj = 40.1;
     System.out.println("Autoboxing");
     System.out.println("Entero Obj = " + entereObj.intValue());
     System.out.println("Flotante Obj = " + flotanteObj.floatValue());
     System.out.println("Doble Obj = " + dobleObj.doubleValue());

//     unboxing
     int entero = entereObj;
     float flotante = flotanteObj;
     double doble = dobleObj;
        System.out.println("\nUnboxing" );
        System.out.println("Entero:  = " + entero);
        System.out.println("Flotante = " + flotante);
        System.out.println("Doble = " + doble);
    }
}

